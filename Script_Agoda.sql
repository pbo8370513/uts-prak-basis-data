CREATE TABLE agoda (
  id INT PRIMARY KEY,
  nama VARCHAR(255),
  email VARCHAR(255) UNIQUE,
  password VARCHAR(255),
  alamat VARCHAR(255),
  kota VARCHAR(255),
  negara VARCHAR(255),
  kode_pos VARCHAR(10),
  no_telp VARCHAR(20),
  tanggal_lahir DATE
);

CREATE TABLE destinasi (
  id INT PRIMARY KEY,
  nama VARCHAR(255) NOT NULL,
  deskripsi VARCHAR(255),
  foto VARCHAR(255)
);

CREATE TABLE perjalanan (
  id INT PRIMARY KEY,
  nama VARCHAR(255) NOT NULL,
  destinasi_id INT,
  mulai_tanggal DATE,
  berakhir_tanggal DATE,
  harga DECIMAL(15,2),
  kapasitas INT,
  FOREIGN KEY (destinasi_id) REFERENCES destinasi(id)
);

CREATE TABLE pemesanan (
  id INT PRIMARY KEY,
  user_id INT,
  perjalanan_id INT,
  tanggal_pemesanan DATE,
  status VARCHAR(255) DEFAULT 'dipesan',
  jumlah_orang INT,
  FOREIGN KEY (user_id) REFERENCES agoda(id),
  FOREIGN KEY (perjalanan_id) REFERENCES perjalanan(id)
);

CREATE TABLE ulasan (
  id INT PRIMARY KEY,
  user_id INT,
  perjalanan_id INT,
  rating INT NOT NULL,
  komentar VARCHAR(255),
  FOREIGN KEY (user_id) REFERENCES agoda(id),
  FOREIGN KEY (perjalanan_id) REFERENCES perjalanan(id)
);

CREATE TABLE penginapan (
  id INT PRIMARY KEY,
  user_id INT,
  nama VARCHAR(255) NOT NULL,
  deskripsi VARCHAR(255),
  foto VARCHAR(255),
  harga DECIMAL(15,2),
  kapasitas INT,
  FOREIGN KEY (user_id) REFERENCES agoda(id)
);

CREATE TABLE kamar (
  id INT PRIMARY KEY,
  penginapan_id INT,
  nama VARCHAR(255) NOT NULL,
  deskripsi VARCHAR(255),
  foto VARCHAR(255),
  harga DECIMAL(15,2),
  kapasitas INT,
  FOREIGN KEY (penginapan_id) REFERENCES penginapan(id)
);

CREATE TABLE fasilitas_kamar (
  kamar_id INT,
  fasilitas VARCHAR(255),
  PRIMARY KEY (kamar_id, fasilitas),
  FOREIGN KEY (kamar_id) REFERENCES kamar(id)
);

CREATE TABLE fasilitas_penginapan (
  penginapan_id INT,
  fasilitas VARCHAR(255),
  PRIMARY KEY (penginapan_id, fasilitas),
  FOREIGN KEY (penginapan_id) REFERENCES penginapan(id)
);

CREATE TABLE metode_pembayaran (
id INT PRIMARY KEY,
nama VARCHAR(255) NOT NULL,
deskripsi VARCHAR(255)
);

CREATE TABLE transaksi (
id INT PRIMARY KEY,
user_id INT,
perjalanan_id INT,
kamar_id INT,
jumlah_kamar INT,
metode_pembayaran_id INT,
tanggal_transaksi DATE,
total_bayar DECIMAL(15,2),
FOREIGN KEY (user_id) REFERENCES agoda(id),
FOREIGN KEY (perjalanan_id) REFERENCES perjalanan(id),
FOREIGN KEY (kamar_id) REFERENCES kamar(id),
FOREIGN KEY (metode_pembayaran_id) REFERENCES metode_pembayaran(id)
);

CREATE TABLE pengalaman (
id INT PRIMARY KEY,
user_id INT,
destinasi_id INT,
nama VARCHAR(255) NOT NULL,
deskripsi VARCHAR(255),
harga DECIMAL(15,2),
kapasitas INT,
foto VARCHAR(255),
FOREIGN KEY (user_id) REFERENCES agoda(id),
FOREIGN KEY (destinasi_id) REFERENCES destinasi(id)
);

CREATE TABLE pendaftaran_pengalaman (
id INT PRIMARY KEY,
user_id INT,
pengalaman_id INT,
tanggal_daftar DATE,
status VARCHAR(255) DEFAULT 'pending',
jumlah_orang INT,
FOREIGN KEY (user_id) REFERENCES agoda(id),
FOREIGN KEY (pengalaman_id) REFERENCES pengalaman(id)
);

CREATE TABLE ulasan_pengalaman (
id INT PRIMARY KEY,
user_id INT,
pengalaman_id INT,
rating INT NOT NULL,
komentar VARCHAR(255),
FOREIGN KEY (user_id) REFERENCES agoda(id),
FOREIGN KEY (pengalaman_id) REFERENCES pengalaman(id)
);

CREATE TABLE diskon (
id INT PRIMARY KEY,
kode_diskon VARCHAR(255) UNIQUE,
persentase_diskon DECIMAL(5,2),
tanggal_mulai DATE,
tanggal_berakhir DATE
);

CREATE TABLE pengguna_diskon (
id INT PRIMARY KEY,
user_id INT,
diskon_id INT,
FOREIGN KEY (user_id) REFERENCES agoda(id),
FOREIGN KEY (diskon_id) REFERENCES diskon(id)
);

INSERT INTO agoda (id, nama, email, password, alamat, kota, negara, kode_pos, no_telp, tanggal_lahir) VALUES
  (1, 'Tatang Sampurna', 'tatang.sampurna@gmail.com', 'password123', 'Jl. Example 123', 'Jakarta', 'Indonesia', '12345', '085123123121', '1990-01-01'),
  (2, 'Supomo Darma', 'supomo.darma@gmail.com', 'password456', 'Jl. Contoh 456', 'Bandung', 'Indonesia', '67890', '085123123122', '1992-03-15'),
  (3, 'Michael Arteta', 'michael.arteta@gmail.com', 'password789', 'Jl. Sample 789', 'Surabaya', 'Indonesia', '54321', '085123123123', '1988-07-20'),
  (4, 'Sarah Arani', 'sarah.arani@gmail.com', 'password123', 'Jl. Testing 123', 'Yogyakarta', 'Indonesia', '13579', '085123123124', '1995-05-10'),
  (5, 'Rani Chelseana', 'rani.chelseana@gmail.com', 'password456', 'Jl. Dummy 456', 'Malang', 'Indonesia', '24680', '085123123125', '1991-11-30'),
  (6, 'Emil Kamil', 'emil.kamil@gmail.com', 'password789', 'Jl. Example 789', 'Bandung', 'Indonesia', '97531', '085123123126', '1987-08-25'),
  (7, 'Michael Jordan', 'michael.jordan@gmail.com', 'password123', 'Jl. Testing 123', 'Surabaya', 'Indonesia', '12345', '085123123127', '1993-02-18'),
  (8, 'Sophia Taslim', 'sophia.taslim@gmail.com', 'password456', 'Jl. Dummy 456', 'Yogyakarta', 'Indonesia', '67890', '085123123128', '1989-06-05'),
  (9, 'Aldrian Rizki', 'aldrian.rizki@gmail.com', 'password789', 'Jl. Example 789', 'Jakarta', 'Indonesia', '54321', '085123123129', '1997-04-12'),
  (10, 'Lionel Messi', 'lionel.messi@gmail.com', 'password123', 'Jl. Testing 123', 'Malang', 'Indonesia', '13579', '085123123130', '1994-09-28');
 
 INSERT INTO perjalanan (id, nama, destinasi_id, mulai_tanggal, berakhir_tanggal, harga, kapasitas) VALUES
  (1, 'Liburan Bali', 1, '2023-07-01', '2023-07-07', 2000000, 10),
  (2, 'Pengalaman Yogya', 2, '2023-08-01', '2023-08-07', 1500000, 8),
  (3, 'Petualangan Malang', 3, '2023-09-01', '2023-09-07', 1800000, 12),
  (4, 'Explore Lombok', 4, '2023-10-01', '2023-10-07', 2200000, 10),
  (5, 'City Tour Bandung', 5, '2023-11-01', '2023-11-07', 1200000, 15),
  (6, 'Jakarta City Escape', 6, '2023-12-01', '2023-12-07', 1000000, 20),
  (7, 'Surabaya Heritage Tour', 7, '2024-01-01', '2024-01-07', 1400000, 10),
  (8, 'Medan Adventure', 8, '2024-02-01', '2024-02-07', 1800000, 12),
  (9, 'Semarang Heritage Walk', 9, '2024-03-01', '2024-03-07', 900000, 15),
  (10, 'Makassar Island Hopping', 10, '2024-04-01', '2024-04-07', 1600000, 8);
 
 INSERT INTO pemesanan (id, user_id, perjalanan_id, tanggal_pemesanan, status, jumlah_orang) VALUES
  (1, 1, 1, '2023-06-15', 'dipesan', 2),
  (2, 2, 2, '2023-06-20', 'dipesan', 3),
  (3, 3, 3, '2023-06-25', 'dipesan', 4),
  (4, 4, 4, '2023-06-30', 'dipesan', 2),
  (5, 5, 5, '2023-07-05', 'dipesan', 3),
  (6, 6, 6, '2023-07-10', 'dipesan', 4),
  (7, 7, 7, '2023-07-15', 'dipesan', 2),
  (8, 8, 8, '2023-07-20', 'dipesan', 3),
  (9, 9, 9, '2023-07-25', 'dipesan', 4),
  (10, 10, 10, '2023-07-30', 'dipesan', 2);

 INSERT INTO ulasan (id, user_id, perjalanan_id, rating, komentar) VALUES
  (1, 1, 1, 5, 'Pengalaman liburan yang luar biasa!'),
  (2, 2, 2, 4, 'Pengalaman yang menyenangkan'),
  (3, 3, 3, 4, 'Lokasi yang indah dan seru untuk petualangan'),
  (4, 4, 4, 3, 'Suasana alam yang menenangkan'),
  (5, 5, 5, 4, 'Kuliner enak di Bandung'),
  (6, 6, 6, 5, 'Pengalaman kota Jakarta yang seru'),
  (7, 7, 7, 4, 'Jejalan di kota Surabaya sangat menyenangkan'),
  (8, 8, 8, 5, 'Petualangan yang menantang di Medan'),
  (9, 9, 9, 3, 'Menikmati keindahan Semarang'),
  (10, 10, 10, 4, 'Pulau-pulau di sekitar Makassar sangat indah');

 INSERT INTO penginapan (id, user_id, nama, deskripsi, foto, harga, kapasitas) VALUES
  (1, 1, 'Hotel Bali Indah', 'Hotel bintang 5 di Bali', 'hotel.jpg', 1500000, 2),
  (2, 2, 'Guesthouse Yogya Asri', 'Guesthouse di pusat kota Yogya', 'guesthouse.jpg', 800000, 4),
  (3, 3, 'Homestay Malang Murah', 'Homestay dengan harga terjangkau di Malang', 'homestay.jpg', 500000, 6),
  (4, 4, 'Villa Lombok Indah', 'Villa mewah dengan pemandangan pantai di Lombok', 'villa.jpg', 2500000, 4),
  (5, 5, 'Hotel Bandung Sejuk', 'Hotel modern di tengah kota Bandung', 'hotel.jpg', 1000000, 2),
  (6, 6, 'Guesthouse Jakarta Nyaman', 'Guesthouse dengan fasilitas lengkap di Jakarta', 'guesthouse.jpg', 700000, 4),
  (7, 7, 'Homestay Surabaya Murah', 'Homestay terjangkau di Surabaya', 'homestay.jpg', 400000, 6),
  (8, 8, 'Villa Medan Indah', 'Villa mewah dengan pemandangan alam di Medan', 'villa.jpg', 2800000, 4),
  (9, 9, 'Hotel Semarang Nyaman', 'Hotel dengan layanan terbaik di Semarang', 'hotel.jpg', 1200000, 2),
  (10, 10, 'Guesthouse Makassar Asri', 'Guesthouse dengan fasilitas lengkap di Makassar', 'guesthouse.jpg', 900000, 4);

 INSERT INTO kamar (id, penginapan_id, nama, deskripsi, foto, harga, kapasitas) VALUES
  (1, 1, 'Suite Room', 'Kamar mewah dengan pemandangan laut', 'suite.jpg', 2000000, 2),
  (2, 1, 'Deluxe Room', 'Kamar nyaman dengan fasilitas lengkap', 'deluxe.jpg', 1500000, 2),
  (3, 2, 'Standard Room', 'Kamar standar dengan kenyamanan terbaik', 'standard.jpg', 700000, 2),
  (4, 2, 'Family Room', 'Kamar keluarga dengan fasilitas lengkap', 'family.jpg', 1000000, 4),
  (5, 3, 'Economy Room', 'Kamar ekonomis dengan pemandangan indah', 'economy.jpg', 300000, 2),
  (6, 3, 'Shared Room', 'Kamar bersama dengan fasilitas umum', 'shared.jpg', 200000, 6),
  (7, 4, 'Luxury Villa', 'Villa mewah dengan fasilitas lengkap', 'luxury.jpg', 3000000, 2),
  (8, 4, 'Private Villa', 'Villa pribadi dengan kolam renang', 'private.jpg', 3500000, 2),
  (9, 5, 'Standard Room', 'Kamar standar dengan fasilitas lengkap', 'standard.jpg', 600000, 2),
  (10, 5, 'Superior Room', 'Kamar superior dengan pemandangan kota', 'superior.jpg', 800000, 2);

 INSERT INTO fasilitas_kamar (kamar_id, fasilitas) VALUES
  (1, 'AC'),
  (1, 'TV'),
  (1, 'Mini Bar'),
  (2, 'AC'),
  (2, 'TV'),
  (3, 'AC'),
  (3, 'TV'),
  (4, 'AC'),
  (4, 'TV'),
  (5, 'AC'),
  (6, 'AC'),
  (6, 'Wi-Fi'),
  (7, 'AC'),
  (7, 'TV'),
  (7, 'Private Pool'),
  (8, 'AC'),
  (8, 'TV'),
  (8, 'Private Pool'),
  (9, 'AC'),
  (9, 'TV'),
  (10, 'AC'),
  (10, 'TV');

 INSERT INTO fasilitas_penginapan (penginapan_id, fasilitas) VALUES
  (1, 'Kolam Renang'),
  (1, 'Spa'),
  (1, 'Restaurant'),
  (2, 'Kolam Renang'),
  (2, 'Wi-Fi'),
  (2, 'Laundry'),
  (3, 'Wi-Fi'),
  (4, 'Kolam Renang'),
  (4, 'Spa'),
  (4, 'Private Beach'),
  (5, 'Wi-Fi'),
  (5, 'Restaurant'),
  (6, 'Wi-Fi'),
  (6, 'Restaurant'),
  (6, 'Gym'),
  (7, 'Wi-Fi'),
  (7, 'Laundry'),
  (7, '24-Hour Front Desk'),
  (8, 'Kolam Renang'),
  (8, 'Spa'),
  (8, 'Private Beach'),
  (9, 'Wi-Fi'),
  (9, 'Restaurant'),
  (10, 'Wi-Fi'),
  (10, 'Restaurant'),
  (10, 'Gym');

 INSERT INTO metode_pembayaran (id, nama, deskripsi) VALUES
  (1, 'Transfer Bank', 'Pembayaran melalui transfer bank'),
  (2, 'Kartu Kredit', 'Pembayaran menggunakan kartu kredit'),
  (3, 'PayPal', 'Pembayaran melalui PayPal'),
  (4, 'Dana', 'Pembayaran melalui dompet digital Dana'),
  (5, 'OVO', 'Pembayaran melalui dompet digital OVO'),
  (6, 'GoPay', 'Pembayaran melalui dompet digital GoPay'),
  (7, 'LinkAja', 'Pembayaran melalui dompet digital LinkAja'),
  (8, 'ShopeePay', 'Pembayaran melalui dompet digital ShopeePay'),
  (9, 'Doku Wallet', 'Pembayaran melalui dompet digital Doku Wallet'),
  (10, 'Klarna', 'Pembayaran melalui Klarna');

 INSERT INTO transaksi (id, user_id, perjalanan_id, kamar_id, jumlah_kamar, metode_pembayaran_id, tanggal_transaksi, total_bayar) VALUES
  (1, 1, 1, 1, 2, 1, '2023-06-20', 4000000),
  (2, 2, 2, 2, 3, 2, '2023-06-25', 4500000),
  (3, 3, 3, 3, 4, 3, '2023-06-30', 6000000),
  (4, 4, 4, 4, 2, 4, '2023-07-05', 5000000),
  (5, 5, 5, 5, 3, 5, '2023-07-10', 3600000),
  (6, 6, 6, 6, 4, 6, '2023-07-15', 2800000),
  (7, 7, 7, 7, 2, 7, '2023-07-20', 4000000),
  (8, 8, 8, 8, 3, 8, '2023-07-25', 4200000),
  (9, 9, 9, 9, 4, 9, '2023-07-30', 5400000),
  (10, 10, 10, 10, 2, 10, '2023-08-04', 5800000);

 INSERT INTO pengalaman (id, user_id, destinasi_id, nama, deskripsi, harga, kapasitas, foto) VALUES
  (1, 1, 1, 'Meditasi di Pantai Bali', 'Nikmati momen kedamaian di pantai Bali', 500000, 10, 'meditasi.jpg'),
  (2, 2, 2, 'Kuliner Jogja Malam', 'Jelajahi kuliner malam di Jogja', 300000, 8, 'kuliner.jpg'),
  (3, 3, 3, 'Petualangan Malang Selatan', 'Petualangan seru di Malang Selatan', 400000, 12, 'petualangan.jpg'),
  (4, 4, 4, 'Snorkeling di Gili Trawangan', 'Jelajahi keindahan bawah laut Gili Trawangan', 600000, 10, 'snorkeling.jpg'),
  (5, 5, 5, 'City Tour Bandung Vintage', 'Jelajahi Kota Bandung dengan nuansa vintage', 250000, 15, 'city-tour.jpg'),
  (6, 6, 6, 'Jakarta Culinary Experience', 'Nikmati pengalaman kuliner di Jakarta', 200000, 20, 'culinary.jpg'),
  (7, 7, 7, 'Surabaya Historical Walk', 'Jelajahi sejarah Kota Surabaya', 350000, 10, 'historical.jpg'),
  (8, 8, 8, 'Medan Street Food Adventure', 'Jelajahi kuliner jalanan Medan', 400000, 12, 'street-food.jpg'),
  (9, 9, 9, 'Semarang Heritage Tour', 'Jelajahi keindahan warisan budaya Semarang', 150000, 15, 'heritage.jpg'),
  (10, 10, 10, 'Makassar Sunset Cruise', 'Nikmati keindahan matahari terbenam di Makassar', 300000, 8, 'sunset-cruise.jpg');

 INSERT INTO pendaftaran_pengalaman (id, user_id, pengalaman_id, tanggal_daftar, status, jumlah_orang) VALUES
  (1, 1, 1, '2023-06-15', 'pending', 2),
  (2, 2, 2, '2023-06-20', 'pending', 3),
  (3, 3, 3, '2023-06-25', 'pending', 4),
  (4, 4, 4, '2023-06-30', 'pending', 2),
  (5, 5, 5, '2023-07-05', 'pending', 3),
  (6, 6, 6, '2023-07-10', 'pending', 4),
  (7, 7, 7, '2023-07-15', 'pending', 2),
  (8, 8, 8, '2023-07-20', 'pending', 3),
  (9, 9, 9, '2023-07-25', 'pending', 4),
  (10, 10, 10, '2023-07-30', 'pending', 2);

 INSERT INTO ulasan_pengalaman (id, user_id, pengalaman_id, rating, komentar) VALUES
  (1, 1, 1, 5, 'Pengalaman meditasi yang luar biasa!'),
  (2, 2, 2, 4, 'Kuliner Jogja sangat lezat'),
  (3, 3, 3, 4, 'Petualangan seru di Malang Selatan'),
  (4, 4, 4, 3, 'Snorkeling di Gili Trawangan sangat indah'),
  (5, 5, 5, 4, 'City tour Bandung yang menyenangkan'),
  (6, 6, 6, 5, 'Pengalaman kuliner di Jakarta yang seru'),
  (7, 7, 7, 4, 'Historical walk di Surabaya sangat berkesan'),
  (8, 8, 8, 5, 'Petualangan kuliner di Medan yang menantang'),
  (9, 9, 9, 3, 'Semarang memiliki keindahan heritage yang unik'),
  (10, 10, 10, 4, 'Makassar sunset cruise yang memukau');

 INSERT INTO diskon (id, kode_diskon, persentase_diskon, tanggal_mulai, tanggal_berakhir) VALUES
  (1, 'SUMMER10', 10.00, '2023-06-01', '2023-06-30'),
  (2, 'HOLIDAY20', 20.00, '2023-07-01', '2023-07-31'),
  (3, 'FREEDOM15', 15.00, '2023-08-01', '2023-08-31'),
  (4, 'GETAWAY25', 25.00, '2023-09-01', '2023-09-30'),
  (5, 'EXPLORE30', 30.00, '2023-10-01', '2023-10-31'),
  (6, 'ADVENTURE15', 15.00, '2023-11-01', '2023-11-30'),
  (7, 'WANDERLUST20', 20.00, '2023-12-01', '2023-12-31'),
  (8, 'RELAX25', 25.00, '2024-01-01', '2024-01-31'),
  (9, 'DISCOVER10', 10.00, '2024-02-01', '2024-02-28'),
  (10, 'ESCAPE15', 15.00, '2024-03-01', '2024-03-31');

 INSERT INTO pengguna_diskon (id, user_id, diskon_id) VALUES
  (1, 1, 1),
  (2, 2, 2),
  (3, 3, 3),
  (4, 4, 4),
  (5, 5, 5),
  (6, 6, 6),
  (7, 7, 7),
  (8, 8, 8),
  (9, 9, 9),
  (10, 10, 10);

 
SELECT * FROM agoda;
SELECT * FROM destinasi;
SELECT * FROM perjalanan;
SELECT * FROM pemesanan;
SELECT * FROM ulasan;
SELECT * FROM penginapan;
SELECT * FROM kamar;
SELECT * FROM fasilitas_penginapan;
SELECT * FROM metode_pembayaran;
SELECT * FROM transaksi;
SELECT * FROM pengalaman;
SELECT * FROM pendaftaran_pengalaman;

UPDATE agoda
SET nama = 'Tatang Saupama'
WHERE id = 1;

UPDATE destinasi
SET nama = 'Bali Paradise'
WHERE id = 1;

UPDATE perjalanan
SET harga = 2500000
WHERE id = 1;

UPDATE pemesanan
SET status = 'selesai'
WHERE id = 1;

UPDATE ulasan
SET rating = 4
WHERE id = 1;
     
UPDATE penginapan
SET harga = 1800000
WHERE id = 1;

UPDATE kamar
SET harga = 800000
WHERE id = 1;
     
UPDATE fasilitas_kamar
SET fasilitas = 'AC, TV, Mini Bar'
WHERE kamar_id = 1;

UPDATE fasilitas_penginapan
SET fasilitas = 'Kolam Renang, Wi-Fi, Laundry'
WHERE penginapan_id = 1;

UPDATE metode_pembayaran
SET deskripsi = 'Pembayaran menggunakan kartu kredit atau transfer bank'
WHERE id = 1;

UPDATE transaksi
SET total_bayar = 3500000
WHERE id = 1;

UPDATE pengalaman
SET harga = 400000
WHERE id = 1;

UPDATE pendaftaran_pengalaman
SET status = 'diterima'
WHERE id = 1;

UPDATE ulasan_pengalaman
SET rating = 5
WHERE id = 1;

UPDATE diskon
SET persentase_diskon = 15.00
WHERE id = 1;

UPDATE pengguna_diskon
SET user_id = 3
WHERE id = 1;

DELETE FROM agoda
WHERE id = 2;

DELETE FROM destinasi
WHERE id = 2;

DELETE FROM perjalanan
WHERE id = 2;

DELETE FROM pemesanan
WHERE id = 2;

DELETE FROM ulasan
WHERE id = 2;

DELETE FROM penginapan
WHERE id = 2;

DELETE FROM kamar
WHERE id = 2;

DELETE FROM fasilitas_kamar
WHERE kamar_id = 2;

DELETE FROM fasilitas_penginapan
WHERE penginapan_id = 2;

DELETE FROM transaksi
WHERE id = 2;

DELETE FROM pengalaman
WHERE id = 2;

DELETE FROM pendaftaran_pengalaman
WHERE id = 2;

DELETE FROM ulasan_pengalaman
WHERE id = 2;

DELETE FROM diskon
WHERE id = 2;

DELETE FROM pengguna_diskon
WHERE id = 2;

-- Menghitung rata-rata harga perjalanan berdasarkan destinasi:
SELECT d.nama AS destinasi, AVG(p.harga) AS rata_rata_harga
FROM perjalanan p
INNER JOIN destinasi d ON p.destinasi_id = d.id
GROUP BY d.nama;

-- Menampilkan nama pengguna (agoda) dan nama perjalanan yang telah dipesan:
SELECT a.nama AS nama_pengguna, p.nama AS nama_perjalanan
FROM agoda a
INNER JOIN pemesanan pe ON a.id = pe.user_id
INNER JOIN perjalanan p ON pe.perjalanan_id = p.id;

-- Menampilkan nama pengguna (agoda) dan nama perjalanan yang telah dipesan, serta mengurutkannya berdasarkan tanggal pemesanan terbaru:
SELECT a.nama AS nama_pengguna, p.nama AS nama_perjalanan, pe.tanggal_pemesanan
FROM agoda a
INNER JOIN pemesanan pe ON a.id = pe.user_id
INNER JOIN perjalanan p ON pe.perjalanan_id = p.id
ORDER BY pe.tanggal_pemesanan DESC;

-- Menampilkan nama pengguna (agoda) dan total harga pemesanan dari perjalanan yang telah dipesan:
SELECT a.nama AS nama_pengguna, SUM(p.harga) AS total_harga
FROM agoda a
INNER JOIN pemesanan pe ON a.id = pe.user_id
INNER JOIN perjalanan p ON pe.perjalanan_id = p.id
GROUP BY a.nama;

-- Menampilkan nama pengguna (agoda) yang belum melakukan pemesanan:
SELECT a.nama AS nama_pengguna
FROM agoda a
LEFT JOIN pemesanan pe ON a.id = pe.user_id
WHERE pe.id IS NULL;

-- Menampilkan nama pengguna (agoda) dan total harga transaksi dari setiap pengguna:
SELECT a.nama AS nama_pengguna, SUM(t.total_bayar) AS total_bayar
FROM agoda a
LEFT JOIN transaksi t ON a.id = t.user_id
GROUP BY a.nama;

-- Menampilkan 5 destinasi dengan harga perjalanan tertinggi:
SELECT d.nama AS destinasi, MAX(p.harga) AS harga_tertinggi
FROM destinasi d
INNER JOIN perjalanan p ON d.id = p.destinasi_id
GROUP BY d.nama
ORDER BY harga_tertinggi DESC
LIMIT 5;

-- Menampilkan rata-rata rating pengalaman berdasarkan destinasi:
SELECT d.nama AS destinasi, AVG(up.rating) AS rata_rata_rating
FROM destinasi d
LEFT JOIN pengalaman pa ON d.id = pa.destinasi_id
LEFT JOIN ulasan_pengalaman up ON pa.id = up.pengalaman_id
GROUP BY d.nama;

-- Menampilkan total diskon yang diterapkan pada setiap transaksi:
SELECT t.id AS id_transaksi, SUM(d.persentase_diskon) AS total_diskon
FROM transaksi t
LEFT JOIN pengguna_diskon pd ON t.user_id = pd.user_id
LEFT JOIN diskon d ON pd.diskon_id = d.id
GROUP BY t.id;

-- Menampilkan pengguna (agoda) yang pernah mengikuti pengalaman di destinasi 'Bali':
SELECT a.nama AS nama_pengguna
FROM agoda a
INNER JOIN pendaftaran_pengalaman pp ON a.id = pp.user_id
INNER JOIN pengalaman p ON pp.pengalaman_id = p.id
INNER JOIN destinasi d ON p.destinasi_id = d.id
WHERE d.nama = 'Bali';

-- Menampilkan pengguna (agoda) yang belum memberikan ulasan untuk setiap pengalaman yang pernah diikutinya:
SELECT a.nama AS nama_pengguna, p.nama AS nama_pengalaman
FROM agoda a
LEFT JOIN pendaftaran_pengalaman pp ON a.id = pp.user_id
LEFT JOIN pengalaman p ON pp.pengalaman_id = p.id
LEFT JOIN ulasan_pengalaman up ON p.id = up.pengalaman_id
WHERE up.id IS NULL;



    

